const express = require('express');
const app = express();
const http = require('http');
var cors = require('cors')
const users = [];
const messages = [];
const corsOpts = {
    origin: '*',
  
    methods: [
      'GET',
      'POST',
    ],
  
    allowedHeaders: [
      'Content-Type',
    ],
  };
  
  
app.use(cors(corsOpts));
  
app.use(function (req, res, next) {
    console.log(req.body) // populated!
    next()
  })
  
app.get('/', (req, res) => {
  // console.log("hello !!")
});
const server = http.createServer(app);
const io = require('socket.io')(server, {
  cors: {
    origin: '*',
  }
});
let adminSocketID = '';
const addUser = ({id, userID, name, ammount}) => {
  userID = userID?.trim().toLowerCase();
  name = name?.trim().toLowerCase();
  if (userID === 'admin') {
    adminSocketID = id;
  }

  const existingUser = users.find((user) => {
      user.userID === userID
  });

  if(existingUser) {
      return{error: "Username is taken"};
  }
  const user = {id, userID, name, ammount};
  console.log('user', user);
  users.push(user);
  // console.log(users);
  return {user};
}
const getUser = (userID) => {
  return users.find((user) => {
    return user.userID === userID
  })
}
const removeUser = (id) => {
  // console.log('socket.id', id)
  const index = users.findIndex((user) => {
      return user.id === id
  });

  if(index !== -1) {
      return users.splice(index,1)[0];
  }
}
function randomIntFromInterval(min, max) { // min and max included 
  return Math.floor(Math.random() * (max - min + 1) + min)
}
const defaultMessage = [
  'what a tremensous Hit!',
  'smacket it out of park',
  'what a shot!',
  'tremendous batting',
  'got you',
  'swagger'
];

io.on('connection', socket => { 
  socket.on('join', (userData, callback) => {
    console.log('userData', userData);
    const ammount = userData.userID !== 'admin' ? randomIntFromInterval(3000, 5000) : 10000;
    const { error, user } = addUser(
      { id: socket.id, userID: userData.userID, name: userData?.name || null, ammount: ammount});
      io.to(socket.id).emit('ammount', ammount);
      socket.broadcast.emit('initData', {
        broadCast_options: defaultMessage, 
        button: null
      });
      if (error) return callback(error);
  });
  socket.on('personalChat', (data) => {
    console.log('[personalChat] ==>>', data);
  });
  socket.on('sendMessage', (data, callback) => {
    console.log('[sendMessage] message', data)
    const msg = data.name?.slice(0, 3) + ': '+ data.msg;
    console.log('msg [sendMessage]', msg)
    socket.broadcast.emit("message", msg.toString());
  });

  socket.on('initData', (message, callback) => {
    console.log('[initData]', message);
    const defaulBroadcastOptions = message.default_options.filter((listItem) => listItem.type === 'text').map((item) => item.value);
    const optionsButton = message.default_options.filter(listItem => (listItem.type === 'button'));
    console.log('defaulBroadcastOptions', defaulBroadcastOptions);
    socket.broadcast.emit('initData', {
      broadCast_options: defaulBroadcastOptions, 
      button: optionsButton[0]
    });
  });

  socket.on('bidding', message => {
    console.log('bidding came', message);
    socket.broadcast.emit('bidding', message);
  });
  socket.on('broadCastMessage', message => {
    console.log('bidding came', message);
    socket.broadcast.emit('broadCastMessage', message);
  });

  socket.on('error', function (e) {
    if (e.error() != 'websocket: close sent') {
      // console.log('An unexpected error occured: ', e.error());
    }
  });

  //data : {msg: "", userData: {name: "", userId: ""}}
  socket.on('customMsg', (data) => {
    const user = getUser(data.userId); // get socketID
    console.log('user [customMsg]', user)
    const msg = user?.name?.slice(0, 3) + ': '+ data.message;
    console.log('[customMsg]', msg)
    socket.except(user.id).emit("message", msg);
  })
  //send all default messages Object
  // title: "",
  // type: "",
  // options_list: []
  socket.except(adminSocketID).emit('getDefaultMessages', messages);
  // get a single message from admin and send it to client
  socket.on('adminMessage', function(message, callback) {
    socket.except(adminSocketID).emit('message', message);
  })
  //bid
  // data {title: "", message:"", coin: };
  socket.on('bid', function(data, callback) {
    socket.except(adminSocketID).emit('bidNotify', data);
  });

  socket.on('disconnect', () => {
    // console.log('disconnect');
    const user = removeUser(socket.id);
    // console.log(user);
  })
});

app.post('/sendBulkMessages', function(req, res) {
  messages = [];
  messages.push(req.body);
});

server.listen(3000, () => {
  console.log('listening on *:3000');
});